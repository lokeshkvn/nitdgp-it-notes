clc;
a = [5,8,4;7,2,8];
supply = [7; 10];
demand = [6, 8, 9];
sumSupply = sum(supply);
sumDemand = sum(demand);
if(sumSupply > sumDemand)
    demand(end+1) = sumSupply - sumDemand;
    a(:,size(a, 2)+1) = 0;
elseif(sumSupply < sumDemand)
    supply(end+1) = sumDemand - sumSupply;
    a(size(a, 1)+1, :) = 0;
end
disp(a);    
lc(a, supply, demand);
nw(a, supply, demand);