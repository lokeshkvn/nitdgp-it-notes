from operator import itemgetter

__author__ = 'Gaurav'

def findMedian(arr, n):
    sort(arr, arr+n)
    return arr[n/2]
def partition(arr, l, r, x):
    for i in range(l,r):
        if arr[i] == x:
           break
    swap(arr[i], arr[r])
    i = l;
    for j in range(l,r):
        if arr[j] <= x:
            swap(arr[i], arr[j])
            i+=1
    swap(arr[i], arr[r])
    return i
}
def getmedian(wells, l, r, k):
    if k > 0 and k <= r - l + 1:
        n = r-l+1
        median = (n+4)/5*[0]
        for i in range(0,n/5):
            median[i] = findMedian(wells+l+i*5, 5)
        if i*5 < n:
            median[i] = findMedian(wells+l+i*5, n%5)
            i+=1
        if i == 1:
            medOfMed = median[i-1]
        else:
            getmedian(median, 0, i-1, i/2);

        pos = partition(wells, l, r, medOfMed)
        if pos-l == k-1:
            return wells[pos]
        if pos-l > k-1:
            return getmedian(wells, l, pos-1, k)
        return getmedian(wells, pos+1, r, k-pos+l-1)

if __name__ == '__main__':
    n = int(input())
    wells = []
    for i in range(0,n):
        wells.append(list(map(int, input().split(' '))))

median = getmedian(wells)
print(median)