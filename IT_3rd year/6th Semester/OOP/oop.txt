1. What is the function of scope resolution operators in C++. Explain with example
Scope resolution operator(::) is used to define a function outside a class or when we want to use a global variable but also has a local variable with same name
Ex: class programming {
public:
  void output();
};
void programming::output() {
  cout << "a";
}

2. What is the use of new and delete operators?
new: allocate memory dynamically
delete: de-allocate memory dynamically

3. What is inline function? Why it is required? What are the differences between inline and macro?
Inline function instructs compiler to insert complete body of the function wherever that function got used in code
It eliminates the overhead incurred by function calls
Inline functions are parsed by the compiler, whereas macros are expanded by the C++ preprocessor. Wherever the compiler finds a call to an inline function, it writes a copy of the compiled function definition whereas The C++ preprocessor implements macros by using simple text replacement

4. What is known as arrays of objects. Give example of how they are used?
Arrays of variables of type "class" is known as "Array of objects"
Example: Classname arr[arr_Size]
arr[i].function()

5. Procedure to Pass Object to Function
class classname
{
	return_type function_name(class_name para1,class_name para2)
	{
	}
}

6. Can any function return objects as arguments? Give example
Yes
class classname
{
	class_name function_name(class_name para2)
	{
		class_name obj_local;
		...
		return obj_local;
	}
}
Ex: 
Complex Add(Complex comp2)
{
	Complex temp;
	temp.real=real+comp2.real;
	temp.imag=imag+comp2.imag;
	return temp;
}

7. What is local class? Give example
A class declared inside a function becomes local to that function and is called Local Class
Ex:
void fun()  
{
	class Test
	{
		..
	};
}

8. Write overload function for ++, ==, &&, ;, =,[],() operators
bool operator ==(const Distance& d)
{
 if(feet == d.feet && inches==d.inches)
 {
	return true;
 }
 return false;
}
bool operator &&(const Boolean& right)
{
	return this->value && right.value;
}
void operator=(const Distance &D )
{ 
 feet = D.feet;
 inches = D.inches;
}
int &operator[](int i)
{
  if( i > SIZE )
  {
	  cout << "Index out of bounds" <<endl; 
	  return arr[0];
  }
  return arr[i];
}
Distance operator()(int a, int b, int c)
{
 Distance D;
 D.feet = a + c + 10;
 D.inches = b + c + 100 ;
 return D;
}

9. Write functions to overload pre increment and post increment operator. How compiler differentiates between them
// overloaded prefix ++ operator
Time operator++ ()  
{
 ++minutes;
 if(minutes >= 60)  
 {
	++hours;
	minutes -= 60;
 }
 return Time(hours, minutes);
}
// overloaded postfix ++ operator
Time operator++( int )         
{
 Time T(hours, minutes);
 ++minutes;                    
 if(minutes >= 60)
 {
	++hours;
	minutes -= 60;
 }
 return T; 
}
If int then post increment else pre increment

10. How the constructor of derived class will be written in case of multiple inheritance?
In case of multiple inheritance, the base class is constructed in the same order in which they appear in the declaration of the derived class
Ex:
class A
{
public: A(int ){..}
}
class B
{
public: B(int ){..}
}
class C:public A, public B
{
public: C(int ,int ,int ):A(int ),B(int ){..}
}

10. How the constructor of derived class will be written in case of multilevel inheritance?
Similarly, in a multilevel inheritance, the constructor will be executed in the order of inheritance
Ex:
class A
{
public: A(int ){..}
}
class B:public A
{
public: B(int ,int ):A(int ){..}
}
class C:public B
{
public: C(int ,int ,int ):B(int ,int ){..}
}

11. What is initialization list? How is an initialization list used in constructors of derived class?
Initializer List is used to initialize data members of a class
The list of members to be initialized is indicated with constructor as a comma separated list followed by a colon
Ex:
class A
{
public: A(int ){..}
}
class B
{
public: B(int ){..}
}
class C:public A, public B
{
public: C(int ,int ,int ):A(int ),B(int ){..}
}

12. What are the basic rules for virtual function?
-The virtual functions must be members of some class
-They cannot be static member
-They are accessed by using object pointer
-A virtual function can be a friend of another class
-A virtual function in a base class must be defined, even though it may not be used
-The prototypes of the base class version of a virtual function and all the derived class versions must be identical. If two functions with the same name have different prototypes, C++ considers them as overloaded functions, and the virtual function mechanism is ignored
-We cannot have virtual constructors, but we can have virtual destructors
-While a base pointer can point to any type of the derived objects, the reverse is not true. That is to say, we cannot use a pointer to a derived class to access an object of the base type
-When a base pointer points to a derived class, incrementing or decrementing it will not make it to point to the next object of the derived class. It is incremented or decremented only relative to its base type. Therefore, we should not use this method to move the pointer to the next object
-If a virtual function is defined in the base class, it need not be necessarily redefined in the derived class. In such cases, calls will invoke the base function

13. What is pure virtual function? Why it is required? How abstract classes can be defined in terms of pure virtual function?
Abstract classes act as expressions of general concepts from which more specific classes can be derived. An object of an abstract class type cannot be created.A class that contains at least one pure virtual function is considered an abstract class
A pure virtual function is a virtual function that is required to be implemented by a derived class if that class is not abstract. It has the notation "= 0" in its declaration
It is required since abstract class cannot be created without it
Ex: 
class class_name
{
public: virtual return_type function_name() = 0;
};

14. Why templates are known as parameterize classes?
Because template is defined with parameters which will be replaced by data types during runtime

15.How templates can be used for member function of a class? Write a program to demonstrate
Ex:
template <class type> class TClass
{
    // constructors, etc  
    template <class type2> type2 myFunc(type2 arg);
};