#include<bits/stdc++.h>
using namespace std;

class A{
    int a;
public:
    A(int b)
    {
        a=b;
    }
};
class B:public A{
    int b;
public:
    B(int c):
        A(c)
        {
            b=c;
        }
    void dis()
    {
        cout<<b;

    }
};
int main()
{
    int n;
    cin>>n;
    B b(n);
    b.dis();
}
