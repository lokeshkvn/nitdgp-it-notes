Management Activities
	-Project Planning
	-Project Monitoring & Controlling
Cost is measured in terms of
	-Time & Effort required to develop it
	-Memory required
	-Size(Lines of code)
COCOMO(Constructive Cost Model): Measures total effort in terms of person months
	Cost in project due to requirements for
		-Software
		-Hardware
		-Human Resources
	The Model
		-Obtain initial estimate of dev effort from estimate of thousand lines of code(KLOC)
		-Determine set of 15 multiplying factors for diff project attributes
		-Adjust effort estimate by multiplying the initial estimate with all multiplying factors
		-The initial estimate is given in person months by Ei=a*(KLOC)^b; a&b are constants
	Types of COCOMO models
		-Basic COCOMO
			-Cost=f(size)=f(KLOC)
			-single-valued, static model
			-E=a*(KLOC)^b PM, D=c*E^d months, E/D=>persons
			-a & b=> organic
		-Intermediate COCOMO
			-Cost=f(KLOC, cost drivers)
			-Cost drivers are for
				-Product
					-Required Software Reliability(RELY)
					-Complexity of product(CPLX)
					-Size of application DB(DATA)
				-Project
					-Use of the s/w tools(TOOLS)
					-Application of modern programming practice(MODP)
					-Required Development(SCEP)
				-Hardware
					-Runtime Constraint(RELY)
					-Memory Constraint(STOR)
					-Volatility of virtual memory(VIRT)
					-Required Turnaround Time(TURN)
				-Personnel
					-Analyst Capability(ACAP)
					-Programming Capability(PCAP)
					-Application Experience(AEXP)
					-Virtual Machine Experience(VEXP)
					-Programming Language Experience(LEXP)
			-Effort(E)=a*(KLOC)^b
			-Effort Adjustment Factor(EAF)=Product of all attributes
			-Modified Effort=E*EAF
		-Advanced COCOMO
			-There are cost drivers at each & every phase of dev
	Types of Software Projects
		-Organic Mode
			-relatively small, simple s/w project
			-similar experience & familiar environment
			Ex: Simple Inventory System
		-Semi-detached
			-Intermediate in size & complexity
			-Teams with experienced & inexperienced levels
			-Have limited experience of related systems & may be unfamiliar with some aspects of the system to be developed
			Ex: Development of new DBMS/OS
		-Embedded Mode
			-Developed with a set of tight h/w, s/w & operational constraint
			-Org has little experience of developing such a system
			Ex: Embedded Avionics system