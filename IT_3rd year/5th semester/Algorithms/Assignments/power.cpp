#include<iostream>
#include<algorithm>
#define ll long long int
using namespace std;

void pw(ll *a, ll *b,ll *c)
{
    ll i;
    *c=1;
    for(i=0;i<*b;i++)
    {
        *c*=*a;
    }
}

void power(ll* a, ll* b,ll* r)
{
    ll c=*b;
    if(*b==0)
    {
        *r=1;
        return ;
    }
    *b/=2;
    power(a,b,r);
    *r = (*r)*(*r);
    if(c%2) *r = (*r)*(*a);
    return ;
}


int main()
{
    ll a,b,c,d;
    cin >> a >> b;
    pw(&a,&b,&c);
    cout << c << "\n";
    power(&a,&b,&d);
    cout << d << "\n";
    return 0;
}
