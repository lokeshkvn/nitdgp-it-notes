
int carry, i, i1, i2, l, l1, l2, l3, k, n, j, n1;
char s[10000], a[10000], b[10000];
char f[5009][1100];
void fibonacci_freeze()
{ 
    a[0] = '0';
    a[1] = '\0';
    b[0] = '1';
    b[1] = '\0';
    n1 = 0;
    for(i = 0; i <= 5000; i++)
    {
        l1 = strlen(a);
        l2 = strlen(b);

        if(l1 > l2)
        {
            for(i1 = l2; i1 < l1; i1++)
            {
                b[i1] = '0';
            }
            b[i1] = '\0';
        }

        else if(l2 > l1)
        {
            for(i2 = l1; i2 < l2; i2++)
            {
                a[i2] = '0';
            }
            a[i2] = '\0';
        }

        int j1 = 0;
        carry = 0;
        l3 = strlen(a);

        for(j = 0; j < l3; j++)
        {
            k = (a[j] - '0') + (b[j] - '0') + carry;
            if(k > 9)
            {
                s[j1] = (k % 10) + '0';
                carry = k / 10;
                j1++;
            }

            else
            {
                s[j1] = k + '0';
                carry = 0;
                j1++;
            }
        }

        if(carry > 0)
        {
            s[j1] = carry + '0';
            s[j1 + 1] = '\0';
        }
        else s[j1] = '\0';

        l = strlen(s);

        strcpy(a, b);
        strcpy(b, s);
        reverse(s, s + l);
        strcpy(f[n1], s);
        n1++;
    }
}
